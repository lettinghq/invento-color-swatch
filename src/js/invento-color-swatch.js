(function() {
    'use strict';

    var inventoColorSwatch = angular.module('invento-color-swatch', [])

    inventoColorSwatch.directive('inventoColorSwatch', ['$timeout', function($timeout) {
        var swatches, isHex, getHex;

        swatches = [
            [{hex : 'FF8A80'}, {hex : 'FF5252'}, {hex : 'FF1744'}, {hex : 'D50000'}, {hex : 'DD2C00'}, {hex : 'FF3D00'}, {hex : 'FF6E40'}, {hex : 'FF9E80'}],
            [{hex : 'FF80AB'}, {hex : 'FF4081'}, {hex : 'F50057'}, {hex : 'C51162'}, {hex : 'AA00FF'}, {hex : 'D500F9'}, {hex : 'E040FB'}, {hex : 'EA80FC'}],
            [{hex : 'B388FF'}, {hex : '7C4DFF'}, {hex : '651FFF'}, {hex : '6200EA'}, {hex : '304FFE'}, {hex : '3D5AFE'}, {hex : '536DFE'}, {hex : '8C9EFF'}],
            [{hex : '82B1FF'}, {hex : '448AFF'}, {hex : '2979FF'}, {hex : '2962FF'}, {hex : '0091EA'}, {hex : '00B0FF'}, {hex : '40C4FF'}, {hex : '80D8FF'}],
            [{hex : '84FFFF'}, {hex : '18FFFF'}, {hex : '00E5FF'}, {hex : '00B8D4'}, {hex : '00BFA5'}, {hex : '1DE9B6'}, {hex : '64FFDA'}, {hex : 'A7FFEB'}],
            [{hex : 'B9F6CA'}, {hex : '69F0AE'}, {hex : '00E676'}, {hex : '00C853'}, {hex : '64DD17'}, {hex : '76FF03'}, {hex : 'B2FF59'}, {hex : 'CCFF90'}],
            [{hex : 'F4FF81'}, {hex : 'EEFF41'}, {hex : 'C6FF00'}, {hex : 'AEEA00'}, {hex : 'FFD600'}, {hex : 'FFEA00'}, {hex : 'FFFF00'}, {hex : 'FFFF8D'}],
            [{hex : 'FFE57F'}, {hex : 'FFD740'}, {hex : 'FFC400'}, {hex : 'FFAB00'}, {hex : 'FF6D00'}, {hex : 'FF9100'}, {hex : 'FFAB40'}, {hex : 'FFD180'}]
        ];

        isHex = function(hex) {
            return /^#?([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/.test(hex);
        };

        getHex = function(color) {
            return (color.hex ? color.hex.replace(/#/g, '') : color.replace(/#/g, ''));
        };

        return {
            restrict: 'A',
            require: 'ngModel',
            link: function ($scope, element, attrs, ngModel) {
                var container, input, selectColor, columnElement, rowElement, size = 15, height = 0, column = 0, row = 0;

                selectColor = function(color) {
                    color = getHex(color);

                    ngModel.$setValidity('required', isHex(color));
                    input.closest('.form-group').toggleClass('has-success', ngModel.$valid).toggleClass('has-error', !ngModel.$valid);

                    if (ngModel.$valid) {
                        for (var column = 0; column < swatches.length; column++) {
                            for (var row = 0; row < swatches[column].length; row++) {
                                swatches[column][row].$el.toggleClass('selected', color == swatches[column][row].hex);
                            }
                        }
                        
                        ngModel.$setViewValue('#' + color);
                        input.val('#' + color);
                    }
                };

                input = angular.element('<input>')
                    .addClass('form-control')
                    .attr({type : 'text', placeholder : 'Colour'})
                    .on('focus', function() {container.css('display', 'block')})
                    .on('focusout', function() {
                        $timeout(function() {
                            container.css('display', 'none');
                        }, 500);
                    })
                    .on('change', function(e) {selectColor(e.currentTarget.value)});

                element.addClass('colour-swatch').append(input);

                container = angular.element('<div>').addClass('colour-swatch-wrapper');

                for (column = 0; column < swatches.length; column++) {
                    if (swatches[column].length > length) {
                        height = swatches[column].length;
                    }

                    columnElement = angular.element('<div>').addClass('colour-swatch-column').css('width', size + 'px');

                    for (row = 0; row < swatches[column].length; row++) {
                        rowElement = angular.element('<div>')
                            .addClass('colour-swatch-cell')
                            .on('click', (function (color) {return function() {selectColor(color)}})(swatches[column][row]))
                            .css({
                                background  : '#' + swatches[column][row].hex,
                                height      : size + 'px'
                            });

                        swatches[column][row].$el = rowElement;

                        columnElement.append(rowElement);
                    }

                    container.append(columnElement);
                }

                container.css({
                    width   : (swatches.length * size + 4) + 'px',
                    height  : (height * size + 4) + 'px',
                    top     : '-' + (height * size + 9) + 'px'
                });

                element.append(container);

                $scope.$watch(attrs.ngModel, function (value, oldValue) {
                    if (typeof value !== 'undefined') {
                        selectColor(value);
                    }
                });
            }
        };
    }]);
})();
